import calendar
import itertools
import json
import logging
import statistics

from dateutil.parser import parse as date_parse
import flask
from flask import Flask, jsonify, request, render_template
import flask.logging
from flask_caching import Cache
import requests


app = Flask(__name__)
log = app.logger


@app.route("/")
def home():
    launches = requests.get("http://soarcast-backend:8000/api/v1/launches").json()
    launch_list = []
    days_list = []
    for launch in launches:
        log.info("Got launch %s", launches[launch]["name"])
        days = requests.get(
            f"http://soarcast-backend:8000/api/v1/launches/{launch}"
        ).json()
        log.debug(days)
        days_list.extend(days.keys())
        launch_list.append({"name": launches[launch]["name"], "days": days})
    days_list = list(set(days_list))
    days_list.sort()
    return render_template("index.html", data=launch_list, days=days_list)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    app.run(debug=True)
